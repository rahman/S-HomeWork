package com.ruoyi.project.homework.leavehomework.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.homework.leavehomework.mapper.SHomeworkMapper;
import com.ruoyi.project.homework.leavehomework.domain.SHomework;
import com.ruoyi.project.homework.leavehomework.service.ISHomeworkService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 留作业Service业务层处理
 * 
 * @author BahetCoder
 * @date 2021-06-02
 */
@Service
public class SHomeworkServiceImpl implements ISHomeworkService 
{
    @Autowired
    private SHomeworkMapper sHomeworkMapper;

    /**
     * 查询留作业
     * 
     * @param id 留作业ID
     * @return 留作业
     */
    @Override
    public SHomework selectSHomeworkById(Long id)
    {
        return sHomeworkMapper.selectSHomeworkById(id);
    }

    /**
     * 查询留作业列表
     * 
     * @param sHomework 留作业
     * @return 留作业
     */
    @Override
    public List<SHomework> selectSHomeworkList(SHomework sHomework)
    {
        return sHomeworkMapper.selectSHomeworkList(sHomework);
    }

    /**
     * 新增留作业
     * 
     * @param sHomework 留作业
     * @return 结果
     */
    @Override
    public int insertSHomework(SHomework sHomework)
    {
        sHomework.setCreateTime(DateUtils.getNowDate());
        return sHomeworkMapper.insertSHomework(sHomework);
    }

    /**
     * 修改留作业
     * 
     * @param sHomework 留作业
     * @return 结果
     */
    @Override
    public int updateSHomework(SHomework sHomework)
    {
        sHomework.setUpdateTime(DateUtils.getNowDate());
        return sHomeworkMapper.updateSHomework(sHomework);
    }

    /**
     * 删除留作业对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteSHomeworkByIds(String ids)
    {
        return sHomeworkMapper.deleteSHomeworkByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除留作业信息
     * 
     * @param id 留作业ID
     * @return 结果
     */
    @Override
    public int deleteSHomeworkById(Long id)
    {
        return sHomeworkMapper.deleteSHomeworkById(id);
    }

    //根据id查询数据
    @Override
    public SHomework selectSHomeworkListById(Long id)
    {
        return  sHomeworkMapper.selectSHomeworkById(id);
    }
}
