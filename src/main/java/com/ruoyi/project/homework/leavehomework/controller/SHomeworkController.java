package com.ruoyi.project.homework.leavehomework.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.homework.leavehomework.domain.SHomework;
import com.ruoyi.project.homework.leavehomework.service.ISHomeworkService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 留作业Controller
 * 
 * @author BahetCoder
 * @date 2021-06-02
 */
@Controller
@RequestMapping("/leavehomework/leavehomework")
public class SHomeworkController extends BaseController
{
    private String prefix = "leavehomework/leavehomework";

    @Autowired
    private ISHomeworkService sHomeworkService;

    @RequiresPermissions("leavehomework:leavehomework:view")
    @GetMapping()
    public String leavehomework()
    {
        return prefix + "/leavehomework";
    }

    /**
     * 查询留作业列表
     */
    @RequiresPermissions("leavehomework:leavehomework:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(SHomework sHomework)
    {
        startPage();
        List<SHomework> list = sHomeworkService.selectSHomeworkList(sHomework);
        return getDataTable(list);
    }

    /**
     * 导出留作业列表
     */
    @RequiresPermissions("leavehomework:leavehomework:export")
    @Log(title = "留作业", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(SHomework sHomework)
    {
        List<SHomework> list = sHomeworkService.selectSHomeworkList(sHomework);
        ExcelUtil<SHomework> util = new ExcelUtil<SHomework>(SHomework.class);
        return util.exportExcel(list, "留作业数据");
    }

    /**
     * 新增留作业
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存留作业
     */
    @RequiresPermissions("leavehomework:leavehomework:add")
    @Log(title = "留作业", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(SHomework sHomework)
    {
        return toAjax(sHomeworkService.insertSHomework(sHomework));
    }

    /**
     * 修改留作业
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        SHomework sHomework = sHomeworkService.selectSHomeworkById(id);
        mmap.put("sHomework", sHomework);
        return prefix + "/edit";
    }

    /**
     * 修改保存留作业
     */
    @RequiresPermissions("leavehomework:leavehomework:edit")
    @Log(title = "留作业", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(SHomework sHomework)
    {
        return toAjax(sHomeworkService.updateSHomework(sHomework));
    }

    /**
     * 删除留作业
     */
    @RequiresPermissions("leavehomework:leavehomework:remove")
    @Log(title = "留作业", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(sHomeworkService.deleteSHomeworkByIds(ids));
    }

    //根据作业ID获取作业信息
    @GetMapping("/getHomeWorkInfo")
    @ResponseBody
    public SHomework getHomeWorkInfo(Long id)
    {
        System.err.println("homeworkinfo:"+id);
        return (SHomework) sHomeworkService.selectSHomeworkListById(id);
    }
}
