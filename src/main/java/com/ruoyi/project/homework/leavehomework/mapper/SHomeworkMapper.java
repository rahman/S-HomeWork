package com.ruoyi.project.homework.leavehomework.mapper;

import java.util.List;
import com.ruoyi.project.homework.leavehomework.domain.SHomework;

/**
 * 留作业Mapper接口
 * 
 * @author BahetCoder
 * @date 2021-06-02
 */
public interface SHomeworkMapper 
{
    /**
     * 查询留作业
     * 
     * @param id 留作业ID
     * @return 留作业
     */
    public SHomework selectSHomeworkById(Long id);

    /**
     * 查询留作业列表
     * 
     * @param sHomework 留作业
     * @return 留作业集合
     */
    public List<SHomework> selectSHomeworkList(SHomework sHomework);

    /**
     * 新增留作业
     * 
     * @param sHomework 留作业
     * @return 结果
     */
    public int insertSHomework(SHomework sHomework);

    /**
     * 修改留作业
     * 
     * @param sHomework 留作业
     * @return 结果
     */
    public int updateSHomework(SHomework sHomework);

    /**
     * 删除留作业
     * 
     * @param id 留作业ID
     * @return 结果
     */
    public int deleteSHomeworkById(Long id);

    /**
     * 批量删除留作业
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteSHomeworkByIds(String[] ids);
}
