package com.ruoyi.project.homework.stuhomework.service;

import java.util.List;
import com.ruoyi.project.homework.stuhomework.domain.StudentHomework;

/**
 * 作业记录Service接口
 * 
 * @author BahetCoder
 * @date 2021-06-02
 */
public interface IStudentHomeworkService 
{
    /**
     * 查询作业记录
     * 
     * @param id 作业记录ID
     * @return 作业记录
     */
    public StudentHomework selectStudentHomeworkById(Long id);

    /**
     * 查询作业记录列表
     * 
     * @param studentHomework 作业记录
     * @return 作业记录集合
     */
    public List<StudentHomework> selectStudentHomeworkList(StudentHomework studentHomework);

    /**
     * 新增作业记录
     * 
     * @param studentHomework 作业记录
     * @return 结果
     */
    public int insertStudentHomework(StudentHomework studentHomework);

    /**
     * 修改作业记录
     * 
     * @param studentHomework 作业记录
     * @return 结果
     */
    public int updateStudentHomework(StudentHomework studentHomework);

    /**
     * 批量删除作业记录
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteStudentHomeworkByIds(String ids);

    /**
     * 删除作业记录信息
     * 
     * @param id 作业记录ID
     * @return 结果
     */
    public int deleteStudentHomeworkById(Long id);
}
