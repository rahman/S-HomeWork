package com.ruoyi.project.homework.stuhomework.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.homework.stuhomework.mapper.StudentHomeworkMapper;
import com.ruoyi.project.homework.stuhomework.domain.StudentHomework;
import com.ruoyi.project.homework.stuhomework.service.IStudentHomeworkService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 作业记录Service业务层处理
 * 
 * @author BahetCoder
 * @date 2021-06-02
 */
@Service
public class StudentHomeworkServiceImpl implements IStudentHomeworkService 
{
    @Autowired
    private StudentHomeworkMapper studentHomeworkMapper;

    /**
     * 查询作业记录
     * 
     * @param id 作业记录ID
     * @return 作业记录
     */
    @Override
    public StudentHomework selectStudentHomeworkById(Long id)
    {
        return studentHomeworkMapper.selectStudentHomeworkById(id);
    }

    /**
     * 查询作业记录列表
     * 
     * @param studentHomework 作业记录
     * @return 作业记录
     */
    @Override
    public List<StudentHomework> selectStudentHomeworkList(StudentHomework studentHomework)
    {
        return studentHomeworkMapper.selectStudentHomeworkList(studentHomework);
    }

    /**
     * 新增作业记录
     * 
     * @param studentHomework 作业记录
     * @return 结果
     */
    @Override
    public int insertStudentHomework(StudentHomework studentHomework)
    {
        studentHomework.setCreateTime(DateUtils.getNowDate());
        return studentHomeworkMapper.insertStudentHomework(studentHomework);
    }

    /**
     * 修改作业记录
     * 
     * @param studentHomework 作业记录
     * @return 结果
     */
    @Override
    public int updateStudentHomework(StudentHomework studentHomework)
    {
        studentHomework.setUpdateTime(DateUtils.getNowDate());
        return studentHomeworkMapper.updateStudentHomework(studentHomework);
    }

    /**
     * 删除作业记录对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteStudentHomeworkByIds(String ids)
    {
        return studentHomeworkMapper.deleteStudentHomeworkByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除作业记录信息
     * 
     * @param id 作业记录ID
     * @return 结果
     */
    @Override
    public int deleteStudentHomeworkById(Long id)
    {
        return studentHomeworkMapper.deleteStudentHomeworkById(id);
    }
}
