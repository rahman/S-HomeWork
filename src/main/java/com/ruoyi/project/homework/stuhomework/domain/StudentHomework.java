package com.ruoyi.project.homework.stuhomework.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 作业记录对象 s_student_homework
 * 
 * @author BahetCoder
 * @date 2021-06-02
 */
public class StudentHomework extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 学生id */
    @Excel(name = "学生id")
    private String studentId;

    /** 学生姓名 */
    @Excel(name = "学生姓名")
    private String studentName;

    /** 作业id */
    @Excel(name = "作业id")
    private Long homeworkId;

    /** 作业标题 */
    @Excel(name = "作业标题")
    private String homeworkTitle;

    /** 作业内容 */
    @Excel(name = "作业内容")
    private String homeworkContent;

    /** 作业评分 */
    @Excel(name = "作业评分")
    private BigDecimal homeworkScore;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setStudentId(String studentId)
    {
        this.studentId = studentId;
    }

    public String getStudentId()
    {
        return studentId;
    }
    public void setStudentName(String studentName)
    {
        this.studentName = studentName;
    }

    public String getStudentName()
    {
        return studentName;
    }
    public void setHomeworkId(Long homeworkId)
    {
        this.homeworkId = homeworkId;
    }

    public Long getHomeworkId()
    {
        return homeworkId;
    }
    public void setHomeworkTitle(String homeworkTitle)
    {
        this.homeworkTitle = homeworkTitle;
    }

    public String getHomeworkTitle()
    {
        return homeworkTitle;
    }
    public void setHomeworkContent(String homeworkContent)
    {
        this.homeworkContent = homeworkContent;
    }

    public String getHomeworkContent()
    {
        return homeworkContent;
    }
    public void setHomeworkScore(BigDecimal homeworkScore)
    {
        this.homeworkScore = homeworkScore;
    }

    public BigDecimal getHomeworkScore()
    {
        return homeworkScore;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("studentId", getStudentId())
            .append("studentName", getStudentName())
            .append("homeworkId", getHomeworkId())
            .append("homeworkTitle", getHomeworkTitle())
            .append("homeworkContent", getHomeworkContent())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("homeworkScore", getHomeworkScore())
            .append("createBy", getCreateBy())
            .append("updateBy", getUpdateBy())
            .toString();
    }
}
