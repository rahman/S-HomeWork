package com.ruoyi.project.homework.stuhomework.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.homework.stuhomework.domain.StudentHomework;
import com.ruoyi.project.homework.stuhomework.service.IStudentHomeworkService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 作业记录Controller
 * 
 * @author BahetCoder
 * @date 2021-06-02
 */
@Controller
@RequestMapping("/stuhomework/stuhomework")
public class StudentHomeworkController extends BaseController
{
    private String prefix = "stuhomework/stuhomework";

    @Autowired
    private IStudentHomeworkService studentHomeworkService;

    @RequiresPermissions("stuhomework:stuhomework:view")
    @GetMapping()
    public String stuhomework()
    {
        return prefix + "/stuhomework";
    }

    /**
     * 查询作业记录列表
     */
    @RequiresPermissions("stuhomework:stuhomework:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(StudentHomework studentHomework)
    {
        startPage();
        List<StudentHomework> list = studentHomeworkService.selectStudentHomeworkList(studentHomework);
        return getDataTable(list);
    }

    /**
     * 导出作业记录列表
     */
    @RequiresPermissions("stuhomework:stuhomework:export")
    @Log(title = "作业记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(StudentHomework studentHomework)
    {
        List<StudentHomework> list = studentHomeworkService.selectStudentHomeworkList(studentHomework);
        ExcelUtil<StudentHomework> util = new ExcelUtil<StudentHomework>(StudentHomework.class);
        return util.exportExcel(list, "作业记录数据");
    }

    /**
     * 新增作业记录
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存作业记录
     */
    @RequiresPermissions("stuhomework:stuhomework:add")
    @Log(title = "作业记录", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(StudentHomework studentHomework)
    {
        return toAjax(studentHomeworkService.insertStudentHomework(studentHomework));
    }

    /**
     * 修改作业记录
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        StudentHomework studentHomework = studentHomeworkService.selectStudentHomeworkById(id);
        mmap.put("studentHomework", studentHomework);
        return prefix + "/edit";
    }

    /**
     * 修改保存作业记录
     */
    @RequiresPermissions("stuhomework:stuhomework:edit")
    @Log(title = "作业记录", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(StudentHomework studentHomework)
    {
        return toAjax(studentHomeworkService.updateStudentHomework(studentHomework));
    }

    /**
     * 删除作业记录
     */
    @RequiresPermissions("stuhomework:stuhomework:remove")
    @Log(title = "作业记录", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(studentHomeworkService.deleteStudentHomeworkByIds(ids));
    }
}
